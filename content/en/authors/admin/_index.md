---
# Display name
title: Gilberto Pereira Sassi

# Username (this should match the folder name)
authors:
- admin

# Is this the primary user of the site?
superuser: true

# Role/position
role: Assistant Professor of Statistics

# Organizations/Affiliations
organizations:
- name: Federal University of Bahia
  url: ""

# Short bio (displayed in user profile at end of posts)
bio: 

interests:
- Statistics
- Item Response Theory
- Time Series Analysis
- Functional Data Analysis

education:
  courses:
  - course: PhD in Statistics
    institution: University of São Paulo
    year: 2016
  - course: MSc in Statistics
    institution: University of São Paulo
    year: 2012
  - course: BSc in  Mathematics
    institution: University of São Paulo
    year: 2008

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:gilberto.sassi@ufba.br'  # For a direct email link, use "mailto:test@example.org".
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/gilberto_sassi
- icon: gitlab
  icon_pack: fab
  link: https://gitlab.com/gilberto-sassi
- icon: github
  icon_pack: fab
  link: https://gitlab.com/gilberto-sassi
# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: files/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
- Researchers
- Visitors
---

Gilberto Pereira Sassi is an assistant professor of Statistics at the Federal University of Bahia. His research interests include Item Response Theory, Time Series Analysis, Functional Data Analysis.
