---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Introduction to Statistics using R and tidyverse"
subtitle: ""
summary: ""
authors: []
tags: []
categories: []
date: 2020-08-01T13:24:11-03:00
lastmod: 2020-08-01T13:24:11-03:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

Introduction to Statistics using R in portuguese.

## Slides

[Day 1](/files/curso-r-tidyverse/aula_1.pdf) <br/>
[Day 2](/files/curso-r-tidyverse/aula_2.pdf) <br/>
[Day 3](/files/curso-r-tidyverse/aula_3.pdf) <br/>
[Day 4](/files/curso-r-tidyverse/aula_4.pdf) <br/>
[Day 5](/files/curso-r-tidyverse/aula_5.pdf)

## Datasets

[dados.xlsx](/files/curso-r-tidyverse/dados.xlsx) <br/>
[iris.csv](/files/curso-r-tidyverse/iris.csv) <br/>
[iris.txt](/files/curso-r-tidyverse/iris.txt) 
