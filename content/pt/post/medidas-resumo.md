---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Estatística descritiva em tabelas de distribuição de frequência"
subtitle: "Tenho a tabela de distribuição de frequência e não tenho os dados? Don't panic, dá pra fazer bastante coisa mesmo assim."
summary: ""
authors: []
tags: []
categories: []
date: 2020-09-17T13:46:50-03:00
lastmod: 2020-10-03T13:46:50-03:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

Você já se deparou com uma situação em que você tem uma tabela de distribuição de frequência de uma variável quantitativa e precisava
fazer um gráfico ou calcular medidas resumo? Se você tem o conjunto de dados que foram usados para construir a tabela de distribuição de frequência, essa tarefa fica simples no `R`, no `Python` e até mesmo em um software de planilha como o `excel`. E se você não tem o conjunto de dados e tem apenas a tabela de distribuição de frequência? *Don't panic*, dá para fazer estatística descritiva mesmo se só tivermos a tabela de distribuição de frequência e não tivermos o conjunto de dados usados para construir essas tabelas.

Para ilustração, vamos usar o conjunto de dados [Companhia MB](/files/sls/companhia_MB.xlsx) que está no livro ***Estatística Básica*** escrito pelo [Prof Pedro Alberto Morettin](https://www.ime.usp.br/~pam/) e pelo Prof. Wilson Bussab. Este conjunto de dados é hipotético e tem fins didáticos. Este conjunto de dados tem as informações de $36$ funcionários da seção de orçamentos da [Companhia MB](/files/sls/companhia_MB.xlsx).

## Variável quantitativa discreta

Imagine que você tem um tabela de distribuição de frequência de uma variável quantitativa discreta $X$ com valores possíveis $x_1, \dots, x_k$ dada por

|X|Frequência|Frequência relativa (proporção)|Porcentagem|
|:----|:----:|:----:|:-----:|
|$x_1$|$n_1$|$f_1 = \\frac{n_1}{n}$|$100\cdot f_1\\%$|
|$x_2$|$n_2$|$f_2 = \\frac{n_2}{n}$|$100\cdot f_2\\%$|
|$\\vdots$|$\\vdots$|$\\vdots$|$\\vdots$|
|$x_k$|$n_k$|$f_k = \\frac{n_k}{n}$|$100\cdot f_k\\%$|
|Total|$n = n_1 + \\cdots + n_k$|$1\\approx f_1 + \\cdots + f_k$|$100\\% \\approx 100\\cdot f_1 + \\cdots + 100\\cdot f_k$|

Note que $n_i, i=1, \dots, k$ é o número de vezes que o valor $x_i, i=1, \dots, k$ aparece na amostra.

Sempre é uma boa ideia usar figuras geométricas para representar informações de uma tabela de distribuição de frequências. No caso de uma variável quantitativa discreta $X$ recomenda-se usar um gráfico de barras, que é simples e provavelmente o leitor já teve contato com um gráfico de barras. Em um gráfico de barras, a informação está na altura das barras, ou seja, para cada valor $x_i, i=1,\dots, k$ desenhamos barras separadas por um espaço no plano cartesiano e a altura da barra é a frequência[^1].

{{< figure src="/files/blog/barras-geral.png" title="Forma de um gráfico de barras." width="50%" alt="Gráfico de barras usando uma tabela de distribuição de frequência." >}}

Para calcular as medidas de resumo, note que o valor $x_i$ aparece $n_i$ vezes para $i=1, \dots, k$. Aqui vamos calcular a média, desvio padrão e a mediana. Para calcular a média e o desvio padrão, somamos todos os valores e dividimos pelo tamanho da amostra, então podemos usar a frequência da tabela de distribuição de frequência como descrito na equação abaixo.
$$
\begin{split}
\bar{x} &= \frac{\overbrace{x_1+ \cdots + x_1}^{n_1\mbox{ vezes}} + \cdots + \overbrace{x_k+ \cdots + x_k}^{n_k\mbox{ vezes}}}{n}\newline
&= \frac{n_1 \cdot x_1 + \cdots + n_k \cdot x_k}{n}\newline
dp(x) = \sqrt{Var(x)} &= \sqrt{\frac{\overbrace{(x_1 - \bar{x})^2+ \cdots + (x_1 - \bar{x})^2}^{n_1\mbox{ vezes}} + \cdots + \overbrace{(x_k - \bar{x})^2+ \cdots + (x_k - \bar{x})^2}^{n_k\mbox{ vezes}}}{n-1}}\newline
&= \sqrt{\frac{n_1\cdot(x_1 - \bar{x})^2 + \cdots + n_k \cdot (x_k - \bar{x})^2}{n-1}}
\end{split}
$$

Para calcular a mediana, precisamos calcular as estísticas de ordem $x_{(k)}$, em que $x_{(k)}$ é o $k$-ésimo menor número. Imagine que $x_1$ é o menor número da amostra, então os $n_1$ menores valores são todos iguais a $x_1$: $x_{(1)} = x_{(2)} = \cdots = x_{(n_1)} = x_1$. Suponha que $x_2$ é o segundo menor valor da amostra, então os $n_2$ menores valores (depois de $x_1$) são todos iguais a $x_2$: $x_{(n_1+1)} = x_{(n_1+2)} = \cdots = x_{(n_1+n_2)} = x_2$. Imagine agora que $x_3$ é o terceiro menor valor da amostra, então os menores valores (depois de $x_1$ e $x_2$) são todos iguais a $x_3$: $x_{(n_1+n_2+1)} = x_{(n_1+n_2+2)} = \cdots = x_{(n_1+n_2+n_3)} = x_3$. E continuamos neste processo até organizar todos os valores observados da amostra do menor até o maior valor como indicado na tabela abaixo. Note que estamos usando a frequência $n_1$, $n_2$ até $n_k$ da tabela distribuição de frequências.

|Estatística de ordem|
|:----------------------|
|$x_{(1)} = x_{(2)} = \cdots = x_{(n_1)} = x_1$|
|$x_{(n_1+1)} = x_{(n_1+2)} = \cdots = x_{(n_1+n_2)} = x_2$|
|$x_{(n_1+n_2+1)} = x_{(n_1+n_2+2)} = \cdots = x_{(n_1+n_2 + n_3)} = x_3$|
|$\\vdots$|
|$x_{(n_1+n_2 + n_3+ \cdots + n_{k-1}+1)} = x_{(n_1+n_2 + n_3+ \cdots + n_{k-1}+2)} = \cdots = x_{(n_1+n_2 + n_3+ \cdots + n_{k-1}+ n_k)} = x_k$|

Então a médiana é calculada do jeito tradicional usando a equação abaixo.

$$
md(x) = \frac{x_{\left(\lceil(n+1)\cdot 0.5\rceil \right)} + x_{\left(\lfloor(n+1)\cdot 0.5\rfloor \right)}}{2}
$$

em que $\lfloor \cdot \rfloor$ é a função _arredondar para baixo_ e $\lceil \cdot \rceil$ é a função *arredondar para cima*.

### Exemplo com o conjunto de dados `Companhia MB` -- `Número de filhos`

Para a variável quantitativa discreta `Número de Filhos`, que vamos representar pela letra maiúscula do alfabeto latino `Z`, contamos o número de funcionários com zero filhos, com um filho, com dois filhos e assim por diante. Como resultado, temos a seguinte tabela de distribuição.

|Número de Filhos | Frequência| Frequência relativa (proporção)| Porcentagem|
|:----------------|:----------:|:-------------------:|:-----------:|
|0                |         20|                0,56|       55,56|
|1                |          5|                0,14|       13,89|
|2                |          7|                0,19|       19,44|
|3                |          3|                0,08|        8,33|
|5                |          1|                0,03|        2,78|
|Total            |         36|                1|      100|

Então em nosso gráfico de barras teremos uma barra para o valor $0$ de altura $20$, uma barra para o valor $1$ de altura $5$, uma barra para o valor $2$ de altura $7$, uma barra para o valor $3$ de altura $3$, e uma barra para o valor $5$ de altura $1$.

{{< figure src="/files/blog/grafico-barras-numero-filhos.png" title="Gráfico de barras para a variável número de filhos" width="50%" alt="Gráfico de barras para a variável número de filhos." >}}

Vamos calcular a média e o desvio para a variável quantitativa discreta `Número de Filhos`, que vamos representar pela letra maiúscula `Z`.
$$
\begin{split}
\bar{z} &= \frac{n_1 \cdot x_1 + \cdots + n_k \cdot x_k}{n} = \frac{20\cdot 0 + 5 \cdot 1 + 7 \cdot 2 + 3 \cdot 3 + 1 \cdot 5}{36} = 0,92\newline
dp(z) &= \sqrt{\frac{n_1\cdot (z_1 - \bar{z})^2 + \cdots + n_k\cdot (z_k - \bar{z})^2}{n-1}} \newline
&= \sqrt{\frac{20\cdot (0 - 11,22)^2 + 5\cdot (1 - 11,22)^2+7\cdot (2 - 11,22)^2 + 3\cdot (3 - 11,22)^2+1\cdot (5 - 11,22)^2}{35}}\newline
&= 1,25
\end{split}
$$

Para calcular a mediana, apresentamos as estatísticas descritivas na tabela abaixo.

|Estatísticas descritivas|
|:--------------------|
|$z_{(1)} = z_{(2)} = \cdots = z_{(20)} = 0$|
|$z_{(21)} = z_{(22)} = \cdots = z_{(25)} = 1$|
|$z_{(26)} = z_{(27)} = \cdots = z_{(32)} = 2$|
|$z_{(33)} = z_{(34)} = z_{(35)} = 3$|
|$z_{(36)} = 5$|

$$
\begin{split}
md(z) &= \frac{z_{\left( \lfloor (n+1)\cdot 0,5 \rfloor \right)} + z_{\left( \lceil (n+1)\cdot 0,5 \rceil \right)}}{2} = \frac{z_{\left( \lfloor (36+1)\cdot 0,5 \rfloor \right)} + z_{\left( \lceil (36+1)\cdot 0,5 \rceil \right)}}{2}\newline
&= \frac{z_{\left( \lfloor 18,5 \rfloor \right)} + z_{\left( \lceil 18,5 \rceil \right)}}{2} = \frac{z_{\left( 18 \right)} + z_{\left( 19 \right)}}{2} = \frac{0 + 0}{2} = 0
\end{split}
$$

## Variável quantitativa contínua

Suponha agora que você tem tabela de distribuição de frequências para uma variável quantitativa contínua. Nesse caso, os valores da variável quantitativa contínua estão agrupados ou agregados em intervalos ou faixas representados por $[a,b)$, onde colchetes ($[$ ou $]$) indicam que o número entra na contagem da frequência ($a$ seria considerado na contagem do número de valores em $[a, b)$) e parênteses ($($ ou $)$) indicam que o número não entra na contagem da frequência ($b$ não seria considerado na contagem do número de valores em $[a, b)$.

|X|Frequência|Frequência Relativa (proporção)|Porcentagem|
|:---:|:---:|:----:|:----:|
|$[a_0, a_1)$| $n_1$|$f_1 =\\frac{n_1}{n}$|$100\\cdot f_1\\%$|
|$[a_1, a_2)$| $n_2$|$f_2 =\\frac{n_2}{n}$|$100\\cdot f_2\\%$|
|$\\vdots$| $\\vdots$|$\\vdots$|$\\vdots$|
|$[a_{k-1}, a_k)$| $n_k$|$f_k =\\frac{n_k}{n}$|$100\\cdot f_k\\%$|
|Total| $n=n_1+\\cdots+n_k$|$1 \\approx n_1 + \cdots + n_k$|$100\\% \\approx 100\\cdot f_1 + \\cdots+ 100\\cdot f_k $|

Quando temos uma tabela de distribuição de frequências para variável quantitativa contínua e não temos o conjunto de dados que foi usado para construir esta tabela de distribuição de frequência, não temos como saber os valores observados da variável quantitativa contínua:  a única informação que temos são as faixas ou intervalos. Uma solução é assumir que os todos os valores que estão em uma faixa ou intervalo é aproximadamente igual ao meio dessa faixa que vamos chamar de *ponto médio* e teríamos a seguinte tabela de distribuição de frequência.

|X|Frequência|Frequência Relativa (proporção)|Porcentagem|Ponto médio
|:---:|:---:|:----:|:----:|:----:|
|$[a_0, a_1)$| $n_1$|$f_1 =\\frac{n_1}{n}$|$100\\cdot f_1\\%$|$\\frac{a_0+a_1}{2}$|
|$[a_1, a_2)$| $n_2$|$f_2 =\\frac{n_2}{n}$|$100\\cdot f_2\\%$|$\\frac{a_1+a_2}{2}$|
|$\\vdots$| $\\vdots$|$\\vdots$|$\\vdots$|$\\vdots$|
|$[a_{k-1}, a_k)$| $n_k$|$f_k =\\frac{n_k}{n}$|$100\\cdot f_k\\%$|$\\frac{a_{k-1}+a_k}{2}$|
|Total| $n=n_1+\\cdots+n_k$|$1 \\approx n_1 + \cdots + n_k$|$100\\% \\approx 100\\cdot f_1 + \\cdots+ 100\\cdot f_k $|$--$|

E podemos usar estes pontos médios para construir um gráfico de barras de forma semelhante ao caso da variável quantitativa discreta conforme a figura abaixo.

{{< figure src="/files/blog/barras-continua.png" width="50%" alt="Gráfico de barras para variáveis quantitativas contínuas" title="Gráfico de barras para variáveis quantitativas contínuas (usando tabela de distribuição de variável quantitativa contínua)." >}}

Note que quando temos uma tabela de distribuição de frequências, os valores estão agregados em faixas ou intervalos e não sabemos e não temos como recuperar os valores individuais da amostra. Além disso, ao usarmos o ponto médio do faixa (ou intervalo) não sabemos mais as larguras das faixas e perdemos mais um pouco de informação. Mas esta última perda de informação pode ser contornada usando histograma.

O histograma é gráfico construído com retângulos cuja base tem largura $a_i - a_{i-1}, i=1, \dots, k$, e área destes retângulos é igual a frequência relativa (ou proporção)[^2]. Para destacar essa diferença entre o histograma e o gráfico de barras, as barras do histograma são desenhadas sem espaço entre elas. Note que no histograma sabemos as larguras das faixas, ao contrário do gráfico de barras, e sempre é melhor usar histograma quando temos uma variável quantitativa contínua.

{{< figure src="/files/blog/histograma-continua.png" width="75%" alt="Histograma" title="Histograma" >}}

Para calcular medidas de resumo, vamos assumir que todos os valores de uma faixa ou intervalo são aproximadamente iguais ao ponto médio e iremos usar uma estratégia semelhante ao gráfico de barra para variável quantitativa contínua quando temos tabela de distribuição de frequências. Vamos começar a definição da média e do desvio padrão na equação abaixo.

$$
\begin{split}
\bar{x} &= \frac{n_1\cdot \frac{a_0+a_1}{2} + n_2\cdot \frac{a_1+a_2}{2} + \cdots + n_k\cdot \frac{a_{k-1}+a_k}{2}}{n}\newline
dp(x) &= \sqrt{ \frac{n_1\cdot \left(\frac{a_0+a_1}{2} - \bar{x} \right)^2 + n_2\cdot  \left(\frac{a_1+a_2}{2} - \bar{x} \right)^2 + \cdots + n_k\cdot \left(\frac{a_{k-1}+a_k}{2} - \bar{x}\right)^2}{n} }
\end{split}
$$

Para calcular a mediana, começamos com as estatísticas de ordem.

|Estatísticas de ordem|
|:-----------------|
|$x_{(1)} = x_{(2)} = \cdots = x_{(n_1)} = \frac{a_0 + a_1}{2}$|
|$x_{(n_1 + 1)} = x_{(n_1 + 2)} = \cdots = x_{(n_1 + n_2)} = \frac{a_1 + a_2}{2}$|
|$x_{(n_1 + n_2 + 1)} = x_{(n_1 + n_2 + 2)} = \cdots = x_{(n_1 + n_2+ n_3)} = \frac{a_2 + a_3}{2}$|
|$\vdots$|
|$x_{(n_1 + n_2 + \cdots + n_{k-1} + 1)} = x_{(n_1 + n_2 + \cdots + n_{k-1} + 2)} = \cdots = x_{((n_1 + n_2 + \cdots + n_{k-1} + n_k))} = \frac{a_{k-1} + a_k}{2}$|

E daí basta usar a definição da mediana.

$$
md(x) = \frac{x_{\left(\lceil(n+1)\cdot 0.5\rceil \right)} + x_{\left(\lfloor(n+1)\cdot 0.5\rfloor \right)}}{2}
$$

em que $\lfloor \cdot \rfloor$ é a função _arredondar para baixo_ e $\lceil \cdot \rceil$ é a função *arredondar para cima*.

### Exemplo com o conjunto de dados `Companhia MB` -- `Salário`

Considere a tabela de distribuição de frequência da variável quantitativa contínua `Salário` na tabela abaixo.

|Salário   | Frequência|   Frequência relativa (proporção)| Porcentagem|
|:-------|:----------:|:----:|:-----------:|
|$[4,8)$   |         10| 0,28|       $27,78\\%$|
|$[8,12)$  |         12| 0,33|       $33,33\\%$|
|$[12,16)$ |          8| 0,22|       $22,22\\%$|
|$[16,20)$ |          5| 0,14|       $13,89\\%$|
|$[20,24]$ |          1| 0,03|        $2,78\\%$|
|Total   |         36| 1,00|      $100,00\\%$|

Para construir o gráfico de barras e o histograma temos que calcular o ponto médio e a densidade de frequência que apresentamos na tabela abaixo.

|Salário   | Frequência|   Frequência relativa (proporção)| Porcentagem| Ponto Médio| Densidade de frequência|
|:-------|:----------:|:----:|:-----------:|:-----------:|:--------------------:|
|$[4,8)$   |         10| 0,2778|     27,7778|           6|               0,0694|
|$[8,12)$  |         12| 0,3333|     33,3333|          10|               0,0833|
|$[12,16)$ |          8| 0,2222|     22,2222|          14|               0,0556|
|$[16,20)$ |          5| 0,1389|     13,8889|          18|               0,0347|
|$[20,24]$ |          1| 0,0278|      2,7778|          22|               0,0069|
|Total   |         36| 1,00|      100,00|          $--$|                   $--$|

Para construir o gráfico de barras, usamos o ponto médio e a frequência conforme a figura abaixo.

{{< figure src="/files/blog/barras-continua-tabela.png" width="50%" alt="Gráfico de barras construído usando uma tabela de distribuição de frequências de variáveis quantitativas contínuas." title="Gráfico de barras construído usando uma tabela de distribuição de frequências de variáveis quantitativas contínuas." >}}

Para construir o histograma, usamos a densidade de frequência.

{{< figure src="/files/blog/histograma-funcionarios.png" width="50%" alt="Histograma para a variável Salário." title="Histograma para a variável quantitativa contínua Salário." >}}

Agora vamos calcular a média e o desvio padrão usando o *ponto médio* das faixas.

$$
\begin{split}
\bar{x} &= \frac{n_1 \frac{a_0 + a_1}{2} + n_2 \frac{a_1 + a_2}{2} + \cdots + n_k \frac{a_{k-1} + a_k}{2}}{n} = \frac{10 \cdot 6 + 12 \cdot 10 + 8 \cdot 14 + 5 \cdot 18 + 1 \cdot 22}{36} \newline
&= 11,22\newline
dp(x) = \sqrt{(Var(x))} &= \sqrt{\frac{n_1 \left(\frac{a_0 + a_1}{2} - \bar{x}\right)^2 + n_2 \left(\frac{a_1 + a_2}{2} - \bar{x}\right)^2 + \cdots + n_k \left(\frac{a_{k-1} + a_k}{2} - \bar{x}\right)^2}{n}}\newline
&= \sqrt{\frac{10\left(6 - 11,22\right)^2 + 12\left(10 - 11,22\right)^2 + 8\left(14 - 11,22\right)^2 + 5\left(18 - 11,22\right)^2 + 1\left(22 - 11,22\right)^2}{n}}\newline
&=4,47
\end{split}
$$

Para calcular a calcular a mediana, precisamos encontrar as estatísticas de ordem.

|Estatísticas de ordem|
|:----------------------|
|$x_{(1)} = x_{(2)} = \cdots = x_{(10)} = 6$|
|$x_{(11)} = x_{(12)} = \cdots = x_{(22)} = 10$|
|$x_{(23)} = x_{(12)} = \cdots = x_{(30)} = 14$|
|$x_{(31)} = x_{(32)} = x_{(33)} = x_{(34)} = x_{(35)} = 18$|
|$x_{(36)} = 22$|

E então podemos calcular a mediana.

$$
\begin{split}
md(x) &= \frac{x_{\left( \lfloor (n+1)\cdot 0,5 \rfloor \right)} + x_{\left( \lceil (n+1)\cdot 0,5 \rceil \right)}}{2} = \frac{x_{\left( \lfloor (36+1)\cdot 0,5 \rfloor \right)} + x_{\left( \lceil (36+1)\cdot 0,5 \rceil \right)}}{2}\newline
&= \frac{x_{\left( \lfloor 18,5 \rfloor \right)} + x_{\left( \lceil 18,5 \rceil \right)}}{2} = \frac{x_{\left( 18 \right)} + x_{\left( 19 \right)}}{2} = \frac{10 + 10}{2} = 10
\end{split}
$$

[^1]: Em um gráfico de barras, também podemos usar a frequência relativa (proporção) ou porcentagem.
[^2]: Em um histograma, também podemos usar a frequência (absoluta) ou porcentagem, mas geralmente usamos frequência relativa.
