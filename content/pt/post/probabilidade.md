---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "O que é e como calcular a probabilidade."
subtitle: ""
summary: ""
authors: []
tags: []
categories: []
date: 2020-10-27T13:33:45-03:00
lastmod: 2020-10-27T13:33:45-03:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

Na natureza, existem dois tipos de fenômenos (ou experimentos): determinísticos e aleatórios. Os fenômenos determinísticos são aqueles que sabemos o resultado do fenômeno antes dele ter sido realizado. Por exemplo, imagine que o fenômeno seja soltar uma pedra de altura de 50 centímetros, então, usando os conhecimento de física que aprendemos no ensino básico, concluímos que essa pedra vai cair em direção ao chão e sabemos o resultado do fenômeno antes mesmo de ele ter sido realizado, e, consequentemente, este é um *fenômeno determinístico*. Por sua vez, os fenômenos aleatórios são aqueles que não conseguimos antever qual vai ser o resultado do fenômeno (ou experimento) antes dele ter sido realizado. Por exemplo, imagine que o fenômeno seja lançar um dado com seis faces: não sabemos qual das faces ficarão expostas para cima antes do lançamento do dado, e, consequentemente, este é um fenômeno aleatório.

Apesar de não conseguirmos antever o resultado de um fenômeno aleatório, geralmente sabemos quais são os resultados possíveis. Por exemplo, no fenômeno aleatório que consiste no lançamento de dado de seis faces sabemos que os resultados possíveis são o conjunto dado por $\\{\mbox{face 1}, \mbox{face 2}, \mbox{face 3}, \mbox{face 4}, \mbox{face 5}, \mbox{face 6}\\}$. Chamamos este conjunto de todos os resultados do fenômeno aleatório de *espaço amostral* e o representamos pela letra maiúscula $\Omega$. Chamamos os subconjuntos do espaço amostral de *evento* e os representamos por letras maiúsculas do alfabeto latino. Apesar de não conseguirmos antecipar o resultado do fenômeno, podemos calcular a plausibilidade de um ponto de um *evento* ser o resultado do fenômeno. Chamamos esta *plausibilidade* de probabilidade.   ~~Neste texto, estou supondo que você conhece a teoria básica de conjuntos e tem conhecimentos matemáticos do ensino básico. Mas nada muito avançado e acredito que você vai entender tudo com tranquilidade.~~

## Como calcular ou encontrar probabilidade

Vamos usar um pouquinho de notação matemática. Imagine que temos interesse em um fenômeno aleatório com espaço amostral $\Omega$. Seja $\mathcal{P}(\Omega)$ o [conjunto das partes](https://pt.wikipedia.org/wiki/Conjunto_de_partes) do espaço amostral, ou seja, o conjunto de todos os subconjuntos do espaço amostral. Queremos encontrar um [função](https://pt.wikipedia.org/wiki/Função_(matemática)) $P:\mathcal{P}(\Omega) \longrightarrow [0,1]$, e $P(A) \in [0,1]$ é a chance do resultado do fenômeno aleatório ser um ponto do evento $A \subset \Omega$[^1]. Chamamos essa função $P$ de *probabilidade*. Existem diversas formas de determinar, encontrar, estimar ou aproximar esta função, neste post vou citar três técnicas: *probabilidade frequentista*, *princípio da equiprobabilidade* e *probabilidade subjetiva*.

### Probabilidade frequentista

A ideia da probabilidade é bastante simples e é baseada no [empirismo](https://pt.wikipedia.org/wiki/Empirismo). Imagine que temos um fenômeno aleatório que podemos repetir quantas vezes quisermos. Se quisermos calcularmos a probabilidade do evento $A \in \mathcal{P}(\Omega)$, repetimos o fenômeno aleatório $N$ vezes ~~N bem grande~~, e anotamos quantas vezes o resultado do fenômeno aleatório é um elemento do evento $A$: $n$. Então, a probabilidade do evento $A$ acontecer é a razão:
$$
P(A) = \dfrac{n}{N}.
$$

Com um exemplo tudo fica mais simples. Imagine que temos um dado simples de seis faces e não viciado, e lançamos esse dados $N=1.000.000$ de vezes ~~claramente eu fiz isso simulando lançamentos de um dado em um computador~~  e anotamos o número de vezes que cada face foi resultado do lançamento, conforme descrito na tabela abaixo.

|Face do dado|Frequência|Frequência relativa|
|:--:|:------:|:------:|
|  1| 166675| $\frac{166675}{1.000.000} = 0,1667$|
|  2| 166806| $\frac{166806}{1.000.000} = 0,1668$|
|  3| 166284| $\frac{166284}{1.000.000} = 0,1663$|
|  4| 166569| $\frac{166569}{1.000.00} = 0,1666$|
|  5| 166556| $\frac{166556}{1.000.000} = 0,1666$|
|  6| 167110| $\frac{167110}{1.000.000} = 0,1671$|

Então, a probabilidade do evento $\\{1\\}$ é $P(\\{1\\}) = \frac{166675}{1.000.000} = 0,1667$, e a probabilidade do evento $A = \\{2, 4, 6\\}$ é $P(A) = \frac{166806 + 166569 + 167110}{1.000.000} = 0,500485$. Note que
* $P(\Omega) = \frac{166675 + 166806 + 166284 + 166569 + 166556 + 167110}{1.000.000} = 1$;
* $P(A^c) = \frac{166675 + 166284 + 166556}{1.000.000} =1 -  \frac{166806 + 166569 + 167110}{1.000.000} = 1 - P(A);$
* Imagine $A_1=\\{1, 2\\}$ e $A_2 =\\{5, 6\\}$, então $A_1 \cap A_2 = \emptyset$ e 
$$
\begin{split}
P(A_1 \cup A_2) &= \frac{166675 + 166806 + 166556 + 167110}{1.000.000}\newline
&= \frac{166675 + 166806}{1.000.000} + \frac{166556 + 167110}{1.000.000}\newline
&= P(A_1) + P(A_2).
\end{split}
$$

### Princípio da equiprobabilidade

O princípio da equiprobabilidade, como o próprio nome sugere, é um postulado ou [axioma](https://pt.wikipedia.org/wiki/Axioma), ou seja, basicamente fazemos uma afirmação que todos consideram razoável mas não é possível checar, provar, demonstrar ou deduzir.

> Imagine um fenômeno aleatório com um número finito $N$ de resultados possíveis, e suponha que cada resultado possível tem a mesma chance de ser o resultado do fenômeno aleatório. Imagine que um evento $A$ tem $n$ elementos, então a probabilidade do evento $A$ é $P(A)=\frac{n}{N}$.
>> Princípio da equiprobabilidade.

De novo, considere o lançamento de um dados simples e não viciado. O espaço amostral neste caso é $\Omega = \\{1, 2, 3, 4, 5, 6\\}$ e temos um número finito de resultados possíveis. Além disso, todas faces tem a mesma chance de ser resultado do lançamento do dado, pois o dado é não viciado. Logo, estamos no contexto do princípio da equiprobabilidade com $N=6$, e a probabilidade do resultado do lançamento ser uma face par é dada por
$$
P(\\{\mbox{face par}\\}) = P(\\{2, 4, 6\\}) = \frac{3}{6} = 0,5.
$$
Observe que neste caso, as probabilidades do evento $A$ usando _probabilidade frequentista_ e _princípio da equiprobabilidade_ estão próximas.

Note que

* $P(\Omega) = \frac{6}{6} = 1$;
* $P(A^c) = \frac{3}{6} =1 -  \frac{3}{6} = 1 - P(A)$;
* Imagine $A_1=\\{1, 2\\}$ e $A_2 =\\{5, 6\\}$, então $A_1 \cap A_2 = \emptyset$ e 
$$
\begin{split}
P(A_1 \cup A_2) &= \frac{4}{6}\newline
&= \frac{2}{6} + \frac{2}{6}\newline
&= P(A_1) + P(A_2).
\end{split}
$$

### Probabilidade de subjetiva

Este método é simples, mas depende do conhecimento específico de um especialista. Suponha que temos um fenômeno aleatório, e considere o evento $A$. Uma pessoa ~~provavelmente um(a) especialista neste tipo de fenômeno aleatório~~ atribui de forma arbitrária um número do intervalo $[0,1]$ para o evento $A$. Não tem lógica, regra ou fórmula para este cálculo: ele chegou neste número usando sua experiência pessoal e seu processo cognitivo ~~que é único e irreplicável~~. Basicamente é um _chute_, mas com mira.

Vamos de novo com o nosso exemplo. Imagine um cidadão acostumado com [jogos de tabuleiro](https://pt.wikipedia.org/wiki/Jogo_de_tabuleiro). De suas intuição construída de centenas de partidas, ele intui que a probabilidade do evento $A = \\{2, 4, 6\\}$ é $50\\%$, ou seja, $P(A) = 0,5$.

Vamos considerar um outro exemplo. Considere um analista de política internacional que está  analisando o contexto de tensão entre a [República Popular Democrática da Coreia](https://pt.wikipedia.org/wiki/Coreia_do_Norte) e os [Estados Unidos da América](https://pt.wikipedia.org/wiki/Estados_Unidos). Este analista afirma que a probabilidade de guerra total entre os dois países é $10\\%$. Não existe dedução ou demonstração para este valor, ele é inteiramente baseado na intuição deste analista de política internacional.

Uma pessoa [_coerente_](https://en.wikipedia.org/wiki/Coherence_(philosophical_gambling_strategy)), atribuiria probabilidade com as seguintes propriedades:

* A probabilidade de qualquer resultado do fenômeno aleatório ser resultado do fenômeno aleatório é 1 ou $100\\%$, ou seja, $P(\Omega) = 1$;
* A probabilidade do complemento ou negação de um evento $A$ ser resultado do fenômenos aleatório é o valor que falta para completar 1 ou $100\\%$ da probabilidade de $A$ ser resultado do fenômeno aleatório, ou seja, $P(A^c) = 1 - P(A)$;
* Se dois eventos não tem intersecção, então a probabilidade dos dois eventos ser resultado do fenômeno aleatório é a soma das probabilidades de cada um deles ser resultado do fenômeno aleatório, ou seja, $P(A \cup B) = P(A) + P(B)$ se $A \cap B = \emptyset$.

### Axiomas de probabilidade

Olha que curioso: se calculamos probabilidade usando _probabilidade frequentista_, _princípio da equiprobabilidade_, _probabilidade subjetiva_, temos as seguintes propriedades:

1. $P(\Omega)=1$;
2. $P(A^c) = 1 - P(A)$ para $A \subset \Omega$;
3. $P(A\cup B) = P(A) + P(B)$ se $A \cap B = \emptyset$.

O brilhante matemático soviético [Andrey Kolmogorov](https://pt.wikipedia.org/wiki/Andrei_Kolmogorov) percebeu esse padrão, e entendeu que seria desejável uma probabilidade ter essas propriedades. Assim, ele introduziu os _axiomas de probabilidade_:

> A função $P:\mathcal{F} \longrightarrow [0,1]$, em que $\mathcal{F} \subset \mathcal{P}(\Omega)$, é probabilidade se as seguintes propriedades são válidas:
>
> 1. $P(\Omega)=1$;
> 2. $P(A^c) = 1 - P(A)$ para $A \subset \Omega$;
> 3. $P(A\cup B) = P(A) + P(B)$ se $A \cap B = \emptyset$;
>
> em que $\mathcal{F}$ é um subconjunto do conjunto das partes $\mathcal{P}(\Omega)$. 

Usamos este subconjunto $\mathcal{F}$, porque para alguns fenômenos aleatórios é impossível calcular probabilidade para todos os eventos: surpreendente, contra-intuitivo e fantástico, não é mesmo? Para explicação mais formal de como escolher o conjunto $\mathcal{F}$, consulte o primeiro capítulo do livro do [_A First Look at Rigorous Probability Theory_](https://www.worldscientific.com/worldscibooks/10.1142/6300).

Já aprendemos determinar ou aproximar probabilidade e definimos os _axiomas de probabilidade_, agora vamos ver algumas das propriedades das probabilidades começando com probabilidade condicional, e, em seguida, vamos apresentar o _Teorema da Probabilidade Total_ e o _Teorema de Bayes_.

## Probabilidade condicional


Suponha que um camarada descobriu que o resultado do fenômeno aleatório está dentro do evento $B$, e ele quer saber a probabilidade de um elemento do evento $A$ ser resultado do fenômeno aleatório. Para não ficar repetindo a frase _ser resultado do fenômeno aleatório_, vamos simplificar: se um elemento do evento $A$ for resultado do fenômeno aleatório, vamos falar que o evento $A$ aconteceu (ou acontece). Neste contexto, a única forma do evento $A$ acontecer é o resultado do fenômeno estiver em $A$ e $B$ simultaneamente $(A \cap B)$, conforme ilustrado na figura abaixo.

{{< figure src="/files/blog/interseccao.png" width="75%" alt="digrama de venn com intersecção" title="Diagrama de Venn com intersecção entre os eventos A e B. A único jeito de A acontecer é se o resultado do fenômeno aleatório estiver dentro de $A \cap B$." >}}

Imagine estamos no contexto em que podemos aplicar o princípio da equiprobabilidade, considere que o evento $B$ tem $n_1$ elementos e que o evento $A \cap B$ tem $n_2$ elementos. Além disso, assuma que o espaço amostral tem $N$ elementos. Note que $n_2 \leq n_1$. Então, temos $n_1$ resultados possíveis do fenômeno aleatório ~~sabemos que B acontece~~, e a probabilidade de $A$ acontecer ~~sabendo que B acontece~~ é dada por
$$
\begin{split}
\frac{n_2}{n_1},
\end{split}
$$
Note que $\frac{n_2}{n_1} = \frac{N}{N}\cdot \frac{n_2}{n_1} = \frac{\frac{n_2}{N}}{\frac{n_1}{N}}$. Observe que estamos no princípio da equiprobabilidade, então $P(A \cap B) = \frac{n_2}{N}$ e $P(B) = \frac{n_1}{N}$. Consequentemente concluímos que a probabilidade de $A$ acontecer se sabemos que $B$ acontece é dada por
$$
\frac{P(A \cap B)}{P(B)}.
$$

Chamamos a probabilidade de $A$ acontecer sabendo que $B$ acontece de _probabilidade condicional_, e em estatística resumimos a sentença _a probabilidade de $A$ acontecer sabendo que $B$ acontece_ por _probabilidade de A dado B_. Usamos a seguinte notação matemática para a _probabilidade de A dado B_: $P(A \mid B)$.

Não precisamos nos restringir ao contexto em que podemos aplicar o princípio da equiprobabilidade, e definimos a probabilidade de $A$ dado $B$ por:

> * $P(A \mid B) = \frac{P(A \cap B)}{P(B)}$, se $P(B) > 0$;
> * Por convenção e conveniência, estabelecemos que $P(A \mid B) = P(A)$ se $P(B) = 0$.

Note que com esta definição de probabilidade condicional temos a seguinte propriedade chamada de *regra do produto*:

> * $P(A \cap B) = P(A \mid B) P(B)$;
> * $P(A \cap B) = P(B \mid A) P(A)$.

Vamos ilustrar as ideias de probabilidade condicional com um exemplo. Imagine cinquenta estudantes distribuídos em duas turmas (Turma $A$ e Turma $B$) conforme a tabela abaixo, e queremos escolhê-los por sorteio para formar uma comissão de estudantes.

|Gênero|Turma A|Turma B|Total|
|:-----:|:-----:|:----:|:----:|
|Masculino|21|16|37|
|Feminino|5|6|13|
|Total|26|24|50|

Vamos especificar alguns eventos que usaremos em nossa notação matemática. Lembre-se que usamos as letras maiúsculas do alfabeto latino para representar eventos.

* $A = \\{\mbox{estudante da turma A}\\}$;
* $B = \\{\mbox{estudante da turma B}\\}$;
* $M = \\{\mbox{estudante se identifica com gênero masculino}\\}$;
* $F = \\{\mbox{estudante se identifica com gênero feminino}\\}$.

Imagine que queremos escolher por sorteio uma aluna para participar de uma comissão. Neste contexto, sabemos que a pessoa selecionada entre os alunos se identificará com o gênero feminino, e temos $13$ pessoas que se identificam com o gênero feminino. Entre estudantes que identificam com o gênero feminino, $5$ são da turma $A$. Então, a probabilidade da aluna ser da turma $A$ é dada por
$$
P(A \mid F) = \frac{5}{13} = 0,38.
$$

Se usarmos a definição de probabilidade condicional, vamos chegar no exato mesmo valor $0,38$. De fato, vamos calcular a probabilidade de $A \cap F$ e $F$ usando o princípio da equiprobabilidade, e depois usar a definição de probabilidade condicional para calcular $P(A \mid F)$:
$$
\begin{split}
P(A \cap F) &= \frac{5}{50} = 0,1\newline
P(F) &= \frac{13}{50} = 0,26\newline
P(A \mid F) &= \frac{P(A \cap F)}{P(F)} = \frac{0,1}{0,26} = 0,38.
\end{split}
$$

## Teorema da probabilidade total

A partir de agora vamos fazer algumas mágicas interessantes usando probabilidade condicional. Imagine que o espaço amostral pode ser dividido em algumas partes $C_1, \dots, C_n$, conforme ilustrado na figura abaixo onde dividimos o espaço amostral em quatro partes.

{{< figure src="/files/img/prob-total.jpg" alt="Teorema da probabilidade total." title="Teorema da probabilidade total.">}}

A probabilidade do evento $A$ é área do quadrado indicada pelo círculo preenchido com a cor azul. Para calcular a probabilidade do evento indicado pelo círculo A, podemos calcular as probabilidades por partes deste círculo

* $P(A \cap C_1)$;
* $P(A \cap C_2)$;
* $P(A \cap C_3)$;
* $P(A \cap C_4)$;

e depois somar essas probabilidades para obter a probabilidade do círculo
$$
P(A) = P(A \cap C_1) + P(A \cap C_2) + P(A \cap C_3) + P(A \cap C_4),
$$
e, então, usando a *regra do produto* em $P(A \cap C_1)$, $P(A \cap C_2)$, $P(A \cap C_3)$ e $P(A \cap C_4)$ temos que 
$$
P(A) = P(A \mid C_1) P(C_1) + P(A \mid C_2) P(C_2) + P(A \mid C_3) P(C_3) + P(A \mid C_4) P(C_4).
$$
Chamamos a coleção dos conjuntos $C_1, \cdots, C_4$ de partição do espaço amostral.

Agora vamos enunciar o Teorema de Probabilidade Total de uma forma geral. Suponha que temos uma participação $C_1, \cdots, C_n$ do espaço amostral e imagine que queremos calcular a probabilidade do evento $A$. Além disso, imagine que conhecemos as probabilidades:

* Probabilidade do evento $C_1$: $P(C_1)$; 
* Probabilidade do evento $A$ dado $C_1$: $P(A \mid C_1)$;
* Probabilidade do evento $C_2$: $P(C_2)$; 
* Probabilidade do evento $A$ dado $C_2$: $P(A \mid C_2)$;
* Probabilidade do evento $C_3$: $P(C_3)$; 
* Probabilidade do evento $A$ dado $C_3$: $P(A \mid C_3)$;

$$
\vdots
$$

* Probabilidade do evento $C_n$: $P(C_n)$;
* Probabilidade do evento $A$ dado $C_n$: $P(A \mid C_n)$;

Então, podemos calcular a probabilidade do evento $A$ através da seguinte equação
$$
P(A) = P(A \mid C_1) P(C_1) + P(A \mid C_2) P(C_2) + \cdots + P(A \mid C_n) P(C_n).
$$

Vamos considerar um exemplo simples para ilustrar o uso do Teorema da Probabilidade Total. Imagine que um produtor artesanal de sorvete compra leite de três fazendas: *fazenda pequena*, *fazenda média* e *fazenda grande*. 20% de todo o leite usado por este produtor vem da *fazenda pequena*, 30% de todo o leite usado por este produtor vem da *fazenda média* e 50% de todo o leite usado por este produtor vem da *fazenda grande*. Uma inspenção surpresa da Ministério da Agricultura, Pecuária e Abastecimento (MAPA) descobriu que a 10% do leite vendido pela *fazenda pequena* estava adulterado, 30% do leite vendido pela *fazenda média* estava adulterado e 20% do leite vendido pela *fazenda grande* estava adulterado. E agora vem a pergunta: quanto leite adulterado este pequeno produtor artesanal de sorvete comprou destas fazendas?

Primeiro vamos definir alguns eventos:
$$
\begin{split}
A &= \{\mbox{Galão adulterado}\};\newline
P &= \{\mbox{Galão pequeno produzido pela fazenda pequena}\};\newline
M &= \{\mbox{Galão médio produzido pela fazenda média}\};\newline
G &= \{\mbox{Galão grande produzido pela fazenda grande}\};
\end{split}
$$
e sabemos que (usamos as informações do MAPA e do produtor artesanal)

|||
|:-----|-------:|
|$P(A \mid P) = 0,1;$|$P(P) = 0,2$|
|$P(A \mid M) = 0,3;$|$P(M) = 0,3$|
|$P(A \mid G) = 0,2;$|$P(G) = 0,5$|

então, usando o Teorema de Probabilidade Total, a proporção (ou probabilidade) de galões adulterados comprados pelo pequeno produtor artesanal de sorvete é dada por
$$
\begin{split}
P(A) &= P(A \mid P) P(P) + P(A \mid M) P(M) + P(A \mid G) P(G),\newline
&= 0,1 \cdot 0,2 + 0,3 \cdot 0,3 + 0,2 \cdot 0,5, \newline
&= 0,21.
\end{split}
$$

## Teorema de Bayes

Finalmente, e não menos importante, temos o Teorema de Bayes, que foi inicialmente descoberto simultaneamente e de forma independente pelo pastor presbiteriano [Thomas Bayes](https://pt.wikipedia.org/wiki/Thomas_Bayes) e pelo matemático francês [Pierre Simon Laplace](https://pt.wikipedia.org/wiki/Pierre-Simon_Laplace) no século XVIII. Apesar de matematicamente simples, as implicações e aplicações do Teorema de Bayes são extremamente relevantes para a Probabilidade, Estatística e o conhecimento científico como um todo.  O Teorema de Bayes nos permite atualizar a probabilidade ou _crença_ em que um evento $A$ será o resultado de um fenômeno aleatório ao coletarmos ou descobrirmos novas informações ou evidências sobre o fenômeno aleatório. 

{{< figure src="/files/blog/thomas-bayes.jpeg" width="50%" alt="Reverendo Thomas Bayes" title="Thomas Bayes: matemático, filósofo e pastor presbiteriano." >}}

Vamos considerar um exemplo. Imagine a prevalência de um doença, que denotaremos por $D_1$, numa população é $20\\%$. Ou seja, sem consultar um paciente, um médico estima que este paciente tem a doencça $D_1$ com probabilidade $0,2$.

{{< figure src="/files/blog/laplace.jpeg" width="50%" alt="matemática francês laplace" title="Pierre Simon Laplace: matemático, filósofo, engenheiro, físco e astrônomo." >}}

Na consulta médica, o paciente descreveu o sintoma $S$. Da literatura especilizada em medicina, o médico sabe que este sintoma está presente em apenas três doenças: $D_1$, $D_2$ e $D_3$. Além disso, o médico sabe que

* a probabilidade de um paciente com a doença $D_1$ apresentar o sintoma $S$ é $20\\%$, ou seja, $P(S \mid D_1) = 0,2$;
* a probabilidade de um paciente com a doença $D_2$ apresentar o sintoma $S$ é $40\\%$, ou seja, $P(S \mid D_2) = 0,4$;
* a probabilidade de um paciente com a doença $D_3$ apresentar o sintoma $S$ é $15\\%$, ou seja, $P(S \mid D_3) = 0,15$.

A literatura especializada em medicina também descreve a prevalência das doenças $D_2$ e $D_3$: $P(D_2) = 0,4$ e $P(D_3) = 0,15$.

Agora vem a pergunta mais importante: após o médico descobrir que o paciente tem o sintoma $S$, qual a probabilidade do paciente tem a doença $D_1$? ~~Lembre que antes da consulta, o médico diria que a probabilidade deste paciente ter a doença $D_1$ é $20\\%$.~~ Ou seja, queremos calcular a seguinte probabilidade
$$
\begin{split}
P(D_1 \mid S),
\end{split}
$$
usando a definição de probabilidade condicional e a regra do produto temos que 
$$
\begin{split}
P(D_1 \mid S) &= \frac{P(D_1 \cap S)}{P(S)},\newline
&= \frac{P(D_1) P(S \mid D_1)}{P(S)},
\end{split}
$$
e para calcular $P(S)$ usamos o Teorema da Probabilidade Total 
$$
\begin{split}
P(D_1 \mid S) &= \frac{P(D_1) P(S \mid D_1)}{P(S)}, \newline
&= \frac{P(D_1) P(S \mid D_1)}{P(D_1) P(S \mid D_1) + P(D_2) P(S \mid D_2) + P(D_3) P(S \mid D_3)}, \newline
&= \frac{0,2 \cdot 0,2}{0,2 \cdot 0,2 + 0,4 \cdot 0,4 + 0,4 \cdot 0,15},\newline
&\approx 0,02
\end{split}
$$

Ou seja, ao descobrir que o paciente tem o sintoma $S$, o médico ~~mudou~~ atualizou a probabilidade do paciente ter a doença $D_1$ de $20\\%$ para $2\\%$.

Agora vamos enunciar o Teorema de Bayes de uma forma geral. Suponha que temos uma participação $C_1, \dots, C_n$ do espaço amostral e considere o evento de interesse $A$. Além disso, imagine que conhecemos as probabilidades:

* Probabilidade do evento $C_1$: $P(C_1)$;
* probabilidade do evento $A$ dado $C_1$: $P(A \mid C_1)$;
* Probabilidade do evento $C_2$: $P(C_2)$;
* probabilidade do evento $A$ dado $C_2$: $P(A \mid C_2)$;
* Probabilidade do evento $C_3$: $P(C_3)$;
* probabilidade do evento $A$ dado $C_3$: $P(A \mid C_3)$;
$$
\vdots
$$
* Probabilidade do evento $C_n$: $P(C_n)$;
* Probabilidade do evento $A$ dado $C_n$: $P(A \mid C_n)$;

Então as probabilidades dos eventos $C_i, i=1, \dots, n$ acontecerem dado que sabemos ou observamos o evento $A$ é dada por
$$
\begin{split}
P(C_1 \mid A) &= \frac{P(C_1) P(A \mid C_1)}{P(C_1) P(A \mid C_1) + \cdots + P(C_n) P(A \mid C_n)}, \newline
P(C_2 \mid A) &= \frac{P(C_2) P(A \mid C_2)}{P(C_1) P(A \mid C_1) + \cdots + P(C_n) P(A \mid C_n)}, \newline
&\vdots \newline
P(C_n \mid A) &= \frac{P(C_n) P(A \mid C_n)}{P(C_1) P(A \mid C_1) + \cdots + P(C_n1) P(A \mid C_n)}.\newline
\end{split}
$$

Vamos fazer mais exemplo para ilustração. Imagine que uma planta industrial tem três linhas de produção, nominalmente $L_1, L_2, L_3$, que produzem lotes de um mesmo equipamento eletrônico. Um lote é considerado inadequado para venda se mais de $10\\%$ dos equipamentos eletrônicos deste lote forem defeituosos. Considere os seguintes eventos

* $L_1 = \\{\mbox{lote vem da linha de produção } L_1 \\}$
* $L_2 = \\{\mbox{lote vem da linha de produção }L_2 \\}$
* $L_3 = \\{\mbox{lote vem da linha de produção }L_3 \\}$
* $D = \\{\mbox{mais de }10\\% \mbox{ equipamentos do lote são defeituosos} \\}$

Os engenheiros que construíram estas três linhas afirmam que 

* a probabilidade da linha de produção $L_1$ produzir um lote com mais de $10\\%$ dos equipamentos defeituosos é $35\\%$, ou seja, $P(D \mid L_1) = 0,35$
* a probabilidade da linha de produção $L_2$ produzir um lote com mais de $10\\%$ dos equipamentos defeituosos é $40\\%$, ou seja, $P(D \mid L_2) = 0,4$
* a probabilidade da linha de produção $L_3$ produzir um lote com mais de $10\\%$ dos equipamentos defeituosos é $32,5\\%$, ou seja, $P(D \mid L_3) = 0,325$
* $50\\%$ dos lotes desta planta industrial são produzidos pela linha de produção $L_1$, ou seja, $P(L_1) = 0,5$
* $30\\%$ dos lotes desta planta industrial são produzidos pela linha de produção $L_2$, ou seja, $P(L_2) = 0,3$
* $20\\%$ dos lotes desta planta industrial são produzidos pela linha de produção $L_3$, ou seja, $P(L_3) = 0,2$

A equipe do controle de qualidade identificou um lote inadequado, e os engenheiros da planta industrial precisam fazer ajustes nas linhas de produção. A melhor estratégia, e começar as análises e ajustes na linha de produção com a maior probabilidade de te ter produzido o lote inadequado. Então, vamos usar o _Teorema de Bayes_ para calcular $P(L_1 \mid D), P(L_2 \mid D)$ e $P(L_3 \mid D)$:

$$
\begin{split}
P(L_1 \mid D) &= \frac{P(L_1) P(D \mid L_1)}{P(D)}\newline
&= \frac{P(L_1) P(D \mid L_1)}{P(L_1) P(D \mid L_1) + P(L_1) P(D \mid L_1) + P(L_3) P(D \mid L_3)}\newline
&= \frac{0,35 \cdot 0,5}{0,35 \cdot 0,5 + 0,4 \cdot 0,3 + 0,2 \cdot 0,325}\newline
&= 0,49\newline
P(L_2 \mid D) &= \frac{P(L_2) P(D \mid L_2)}{P(D)}\newline
&= \frac{P(L_2) P(D \mid L_2)}{P(L_1) P(D \mid L_1) + P(L_1) P(D \mid L_1) + P(L_3) P(D \mid L_3)}\newline
&= \frac{0,4 \cdot 0,3}{0,35 \cdot 0,5 + 0,4 \cdot 0,3 + 0,2 \cdot 0,325}\newline
&= 0,33\newline
P(L_3 \mid D) &= \frac{P(L_3) P(D \mid L_3)}{P(D)}\newline
&= \frac{P(L_3) P(D \mid L_3)}{P(L_1) P(D \mid L_1) + P(L_1) P(D \mid L_1) + P(L_3) P(D \mid L_3)}\newline
&= \frac{0,2 \cdot 0,325}{0,35 \cdot 0,5 + 0,4 \cdot 0,3 + 0,2 \cdot 0,325}\newline
&= 0,18
\end{split}
$$

Ou seja, a linha de produção com maior probabilidade de ter produzido o lote inaquedo é a linha de produção $L_1$, $P(L_1 \mid D) = 0,49$, e os engenheiros deveriam começar a análise nesta linha de produção.

[^1]: Dependendo do espaço amostral, não é possível atribuir chances para todos os subconjuntos do espaço amostral. Para mais detalhes, recomendamos um leitura do livro [Probability and Measure](https://www.amazon.com/Probability-Measure-Patrick-Billingsley/dp/1118122372).
