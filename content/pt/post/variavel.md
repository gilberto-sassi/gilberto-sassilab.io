---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Variável aleatória: a conexão entre os dados e a Teoria de Probabilidade"
subtitle: ""
summary: ""
authors: []
tags: []
categories: []
date: 2021-02-10T14:05:33-03:00
lastmod: 2021-02-10T14:05:33-03:00
featured: false
draft: true

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

Neste _post_ eu vou fazer o seguinte: eu vou começar definindo matematicamente e formalmente uma variável aleatória, e depois eu vou mostrar as aplicações usando variáveis aleatórias discretas e variáveis aleatórias contínuas. Antes de continuar, recomendo que você leia o _post_ deste blog sobre [probabilidade](https://gilberto-sassi.netlify.app/pt/post/probabilidade/), e se você estiver muito empolgado eu recomendo que você leia o livro [Probabilidade e variáveis aleatórias](https://www.edusp.com.br/loja/produto/939/probabilidade-e-variaveis-aleatorias). Então vamos lá!

Considere um fenômeno aleatório om espaço amostral $\Omega$ e função de probabilidade $P: \mathcal{P}(\Omega) \longrightarrow [0,1]$ [^1], em que $\mathcal{P}(\Omega)$ é o conjunto de todos os subconjuntos de $\Omega$ e é chamado de [_conjunto de partes_](https://pt.wikipedia.org/wiki/Conjunto_de_partes). Variável aleatória é uma forma de conectar os dados (valores observados de uma amostra) e a probabilidade. Mais precisamente, variável aleatória é uma função $X: \Omega \longrightarrow \mathbb{R}$. No domínio, temos o espaço amostral $\Omega$ e conseguimos calcular a probabilidade do evento $A = \\{\omega \in \Omega \mid X(\omega) \in B\\}$ em que $B \subset \mathbb{R}$, no contra-domínio temos os números que podemos coletar em nosso dia-a-dia e a função relaciona ou _liga_ os pontos do espaço amostral ~~onde conseguimos atribuir ou calcular probabilidades~~ e os números reais ~~que coletamos em situação práticas~~.

Vamos ilustrar o conceito de variável aleatória anterior com um exemplo. Considere o fenômeno aleatório que consiste em três lançamentos ~~independentes~~ de moedas justas (ou não viciadas). Para este fenômeno aleatório, o espaço amostral é


$$
\Omega = \\{CCC, CCK, CKC, CKK, KCC, KCK, KKC, KKK\\},
$$

em que $C$ representa a face cara da moeda e $K$ representa a face coroa da moeda, e cada um dos pontos do espaço amostral tem a mesma chance de ser o resultado do fenômeno aleatório, então podemos usar o princípio da equiprobabilidade.

[^1]: Dependendo do espaço amostral, não é possível atribuir chances para todos os subconjuntos do espaço amostral (conjunto de partes do espaço amostral). Para mais detalhes, recomendamos um leitura do livro [A First Look at Rigorous Probability Theory](http://probability.ca/jeff/grprobbook.html). 
