---
# Display name
title: Gilberto Pereira Sassi

# Username (this should match the folder name)
authors:
- admin

# Is this the primary user of the site?
superuser: true

# Role/position
role: Professor

# Organizations/Affiliations
organizations:
- name: Universidade Federal da Bahia
  url: ""

# Short bio (displayed in user profile at end of posts)
bio: My research interests include distributed robotics, mobile computing and programmable matter.

interests:
- Estatística Matemática
- Séries Temporais
- Dados Funcionais

education:
  courses:
  - course: Doutorado em Estatística
    institution: Universidade de São Paulo
    year: 2016
  - course: Mestrado em Estatística
    institution: Universidade de São Paulo
    year: 2012
  - course: Bacharelado em Matemática
    institution: Universidade de São Paulo
    year: 2008

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:gilberto.sassi@ufba.br'  # For a direct email link, use "mailto:test@example.org".
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/gilberto_sassi
- icon: gitlab
  icon_pack: fab
  link: https://gitlab.com/gilberto-sassi
- icon: github
  icon_pack: fab
  link: https://github.com/gilberto-sassi
# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: files/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
- Researchers
- Visitors
---

Gilberto Pereira Sassi é professor do Departamento de Estatística do Instituto de Matemática e Estatística da Universidade Federal da Bahia.
