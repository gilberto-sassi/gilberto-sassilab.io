+++
# Experience widget.
widget = "experience"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 40  # Order that this section will appear.

title = "Experiência"
subtitle = ""

# Date format for experience
#   Refer to https://sourcethemes.com/academic/docs/customization/#date-format
date_format = "Jan 2006"

# Experiences.
#   Add/remove as many `[[experience]]` blocks below as you like.
#   Required fields are `title`, `company`, and `date_start`.
#   Leave `date_end` empty if it's your current employer.
#   Begin/end multi-line descriptions with 3 quotes `"""`.
[[experience]]
  title = "Professor"
  company = "Universidade Federal da Bahia"
  company_url = "http://ime.ufba.br"
  location = "Salvador, BA"
  date_start = "2017-01-01"
  date_end = ""


[[experience]]
  title = "Professor"
  company = "Universidade Federal Fluminense"
  company_url = "http://ime.uff.br"
  location = "Niterói, RJ"
  date_start = "2016-03-01"
  date_end = "2017-08-31"
  #description = ""

[[experience]]
    title = "Analista"
    company = "Banco Bradesco"
    company_url = "https://banco.bradesco/"
    location = "Osasco, SP"
    date_start = "2011-10-01"
    date_end = "2012-06-30"
    description = """Modelos para recuperação de crédito."""

[[experience]]
    title = "Analista"
    company = "Banco Itaú"
    company_url = "https://banco.bradesco/"
    location = "São Paulo, SP"
    date_start = "2010-06-01"
    date_end = "2011-01-31"
    description = """Validação de modelos no contexto dos acordos de Basiléia."""

+++
