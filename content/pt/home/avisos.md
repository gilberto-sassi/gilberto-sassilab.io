+++
# A section created with the Blank widget.
widget = "blank"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 50  # Order that this section will appear.

# Note: a full width section format can be enabled by commenting out the `title` and `subtitle` with a `#`.
title = "Avisos e comunicados"
subtitle = ""

[design]
  # Choose how many columns the section has. Valid values: 1 or 2.
  columns = "1"

[design.background]
  # Apply a background color, gradient, or image.
  #   Uncomment (by removing `#`) an option to apply it.
  #   Choose a light or dark text color by setting `text_color_light`.
  #   Any HTML color name or Hex value is valid.

  # Background color.
  # color = "navy"
  
  # Background gradient.
  # gradient_start = "DeepSkyBlue"
  # gradient_end = "SkyBlue"
  
  # Background image.
  # image = "image.jpg"  # Name of image in `static/media/`.
  # image_darken = 0.6  # Darken the image? Range 0-1 where 0 is transparent and 1 is opaque.

  # Text color (true=light or false=dark).
  # text_color_light = true

[design.spacing]
  # Customize the section spacing. Order is top, right, bottom, left.
  # padding = ["0px", "0px", "0px", "0px"]

[advanced]
 # Custom CSS. 
 css_style = ""
 
 # CSS class.
 css_class = ""
+++

*********

Devido a decisão do Conselho Universitário da UFBA do dia 18/03/2020, e seguindo as recomendações da Organização Mundial da Saúde, do Ministério da Saúde e da Secretária Estatudal de Saúde do Governo Estadual da Bahia, informo que todas as atividades acadêmicas e o calendário acadêmico estão suspensos por tempo indeterminado devido a pandemia global do vírus COVID-19. Ou seja, as aulas para as seguintes turmas

* Turma 04 (08h50min até 10h30min) de MAT236 (Métodos Estatísticos);
* Turma 05 (10h40min até 12h30min) de MAT236 (Método Estatísticos);
* Turma 01 (13h55min até 16h55min) de MATD03 (Estatística Aplicada à Saúde).

e as aulas da atividade de extensão com título ***Introdução à Estatística Básica usando `R`: seja bem-vind@ ao tidyverse*** estão canceladas.

Para maiores informações consulte o comunicado completo no *UFBA em pauta*: [UFBA interrompe atividades](https://www.ufba.br/ufba_em_pauta/ufba-interrompe-atividades-por-tempo-indeterminado-em-combate-ao-coronavirus).

*********
