---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "GeoFKF"
subtitle: ""
summary: ""
authors: []
tags: []
categories: []
date: 2021-04-29T15:37:08-03:00
lastmod: 2021-04-29T15:37:08-03:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

# geoFKF

The goal of geoFKF is to implement a kriging method for spatial
functional data.

## Installation

You can install the development version of geoFKF from
[GitHub](https://github.com/) using `devtools` package.

``` r
# install.packages("devtools")
devtools::install_github("gilberto-sassi/geoFKF")
```

## Example

This is a basic example which shows you how to solve a common problem:

``` r
library(ggplot2)
library(geoFKF)
data("datasetCanada")

m_data <- as.matrix(datasetCanada$m_data)
m_coord <- as.matrix(datasetCanada$m_coord[, 1:2])
pos <- 18
log_pos <- !(seq_len(nrow(m_coord)) %in% pos)
new_loc <- m_coord[pos, ]
m_coord <- m_coord[log_pos, ]
y_true <- m_data[, pos]
m_data <- m_data[, log_pos]

x <- seq(from = -pi, to = pi, length.out = length(y_true))

fit <- geo_fkf(m_data, m_coord, new_loc, t = x)

df <- data.frame(x , y_true, y_est = fit$estimates)

ggplot(df) +
  geom_line(aes(x, y_true), color = "red") +
  geom_line(aes(x, y_est), color = "blue")
```

![Estimated curve](/files/img/README-example-1.png)