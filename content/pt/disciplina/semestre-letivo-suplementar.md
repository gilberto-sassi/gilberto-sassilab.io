---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Semestre Letivo Suplementar"
subtitle: ""
summary: ""
authors: []
tags: []
categories: []
date: 2020-08-03T15:18:03-03:00
lastmod: 2020-08-03T15:18:03-03:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

Cronograma do curso.

[cronograma](/files/sls/Plano_MAT236_SLS_2020.pdf)

## Slides

### Primeira, segunda e terceira unidade

[Primeiro dia e apresentação do curso](/files/sls/primeiroDia.pdf)

[Gráficos](/files/sls/graficos.pdf)

[Medidas de Resumo](/files/sls/medidasResumo.pdf)

### Quarta unidade

[Introdução à probabilidade](/files/sls/introProb.pdf)

[Variáveis aleatórias discretas](/files/sls/varDiscreta.pdf)

[Variáveis aleatórias contínuas](/files/sls/varContinua.pdf)

### Quinta unidade 

[Intervalo de confiança](/files/sls/intervaloConfianca.pdf)

[Teste de Hipóteses](/files/sls/testeHipoteses.pdf)

## Alguns livros

[Estatística Aplicada às  Ciências Sociais](/files/sls/BARBETTA.pdf)

[Estatística básica](/files/sls/morettin.pdf)

[Applied Statistics and Probability for Engineers](/files/sls/montgomery.pdf)

[Applied Statistics for Civil and Environmental Engineers](/files/sls/Kottegoda.pdf)

## Lista de exercícios

[Lista de Estatística Descritiva](/files/sls/lista_est_descr.pdf)

[Lista de Probabilidade e variáveis aleatórias](/files/sls/lista_prob.pdf)

[Lista de Intervalo de Confiança](/files/sls/lista-ex-ic.pdf)

[Lista de Teste de Hipóteses](/files/sls/lista-test-1-pop.pdf)

## Conjunto de dados

[companhia_MB](/files/sls/companhia_MB.xlsx)

## Tabelas

[Tabela da distribuição normal](/files/sls/tabela-z.pdf)

[Tabela da distribuição qui-quadrado](/files/sls/tabela-chi-square.pdf)

[Tabela da distribuição t-Student](/files/sls/tabela-t-student.pdf)

## Aulas

### Aula dia 15/09/2020

[Notas da aula](/files/sls/aula-mat236-15-09-2020.pdf)

[Exercício 8 da Lista 1](/files/sls/exercicio_8_lista_1.xlsx)

Neste aula de hoje, usamos uma calculadora criada pela Nexo jornal para motivar o conceito de **Quantil**. Você checar essa calculadora nesse link: [Calculadora de Quantil do jornal Nexo](https://www.nexojornal.com.br/interativo/2016/01/11/O-seu-salário-diante-da-realidade-brasileira).

### Aula dia 22/09/2020

[Notas de aula](/files/sls/aula-22-09-2020.pdf)

[Explicação de histograma e exercício 7 da lista 1](/files/sls/aula-22-09-2020.xlsx)

### Aula do dia 29/09/2020

[Notas de aula](/files/sls/aula-mat236-29-09-2020.pdf)

### Aula do dia 06/10/2020

[Notas de aula](/files/sls/Note-06-10-2020.pdf)

### Aula do dia 13/10/2020

[Notas de aula](/files/sls/aula-dia-13-10-20202.pdf)

### Aula do dia 27/10/2020

[Notas de aula](/files/sls/aula-dia-27-10-20202-discr.pdf)

### Aula do dia 03/11/2020

[Notas de aula](/files/sls/aula-dia-27-10-20202-cont.pdf)

[Usando a tabela da distribuição normal](/files/sls/usando-tabela-normal.pdf)

## Nota da primeira (sem nome e com matrícula)

[Nota da primeira prova sem nome e com matrícula](/files/sls/primeira-prova-sem-nome.xlsx)

## Gabarito da primeira prova

[Gabarito da primeira prova](/files/sls/gabarito.pdf)

### Notas de aula do dia 10/11/2020

[Notas de aula do dia 10/11/2020](/files/sls/aula-dia-10-11-2020.pdf)

[Tabela Z comentada e usada na sala de aula](/files/sls/tabela-z.pdf.pdf)

### Notas de aula do dia 17/11/2020

[Notas de aula do dia 17/11/2020](/files/sls/aula-dia-17-11-2020.pdf)

[Tabela Z comentada e usada na sala de aula](/files/sls/tabela-z-17-11-2020.pdf)

[Tabela t comentada e usada na sala de aula](/files/sls/tabela-t-student-17-11-2020.pdf)

### Notas de aula do dia 24/11/2020

[Notas de aula do dia 24/11/2020](/files/sls/notas-24-11-2020.pdf)

[Tabela Z comentada e usada na aula do dia 24/11/2020](/files/sls/tabela-z-24-11-2020.pdf)

[Tabela t comentada e usada na aula do dia 24/11/2020](/files/sls/tabela-t-student-24-11-2020.pdf)

[Tabela Qui-quadrado comentada e usada na aula do dia 24/11/2020](/files/sls/tabela-chi-square-24-11-2020.pdf)

### Notas de aula do dia 01/12/2020

[Notas de aula do dia 01/12/2020](/files/sls/aula-dia-01-12-2020.pdf)

### Notas de aula do dia 08/12/2020

[Notas de aula do dia 08/12/2020](/files/sls/aula-08-12-2020.pdf)

## Gabarito da segunda prova

[Gabarito da segunda prova](/files/sls/gabarito-segunda-prova.pdf)

## Notas finais

[Notas finais sem nome](/files/sls/nota-final-sem-nomel.xlsx)