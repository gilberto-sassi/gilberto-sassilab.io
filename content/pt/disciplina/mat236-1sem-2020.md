---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Métodos Estatísticos"
subtitle: ""
summary: ""
authors: []
tags: []
categories: []
date: 2020-08-03T15:23:17-03:00
lastmod: 2020-08-03T15:23:17-03:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

## Conteúdo programático

[Programa do curso.](/files/mat236/mat236.pdf)

## Slides

[Aula 1](/files/mat236/aula_1.pdf) <br>
[Aula 2](/files/mat236/aula_2.pdf) <br>
[Aula 3](/files/mat236/aula_3.pdf) <br>
[Aula 4](/files/mat236/aula_4.pdf) <br>
[Aula 5](/files/mat236/aula_5.pdf) <br>
[Tabela da distribuição normal](/files/mat236/tabela-z.pdf)

## Listas de exercício

[Primeira lista de exercício](/files/mat236/lista-1.pdf) <br>
[companhia_MB.xlsx](/files/mat236/companhia_MB.xlsx)

## Controle de presença

### Turma 04: 08h50min às 10h50min

[Contagem de faltas --  dia 05/05/2020](/files/mat236/mat236-t04-presenca.xlsx)

### Turma 05: 10h40min às 12h40min

[Contagem de faltas --  dia 05/05/2020](/files/mat236/mat236-t05-presenca.xlsx)
