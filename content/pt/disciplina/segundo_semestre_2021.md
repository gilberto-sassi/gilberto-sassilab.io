---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Segundo semestre de 2021"
subtitle: "Ensino remoto devido a pandemia de COVID-19"
summary: ""
authors: []
tags: []
categories: []
date: 2021-06-22T17:13:17-03:00
lastmod: 2021-06-22T17:13:17-03:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

-----

# Estatística Computacional (Código MATD46)

Material didático de apoido e de suporte a disciplina **MATD46** com título **Estatística Computacional**.

## Slides

### Primeiro dia 

* [Primeiro dia](/files/2sem2021/slides/primeiro_dia.html)
* [Primeiro dia](/files/2sem2021/slides/primeiro_dia.pdf)

### Tópico 1

* [Slides do tópico 1 -- HTML](/files/2sem2021/slides/topico_1.html)
* [Slides do tópico 1 -- PDF](/files/2sem2021/slides/topico_1.pdf)

### Tópico 2

* [Slides do tópico 2 -- HTML](/files/2sem2021/slides/topico_2.html)
* [Slides do tópico 2 -- PDF](/files/2sem2021/slides/topico_2.pdf)

### Tópico 3

* [Slides do tópico 3 -- HTML](/files/2sem2021/slides/topico_3.html)
* [Slides do tópico 3 -- PDF](/files/2sem2021/slides/topico_3.pdf)

### Tópico 4

* [Slides do tópico 4 -- HTML](/files/2sem2021/slides/topico_4.html)
* [Slides do tópico 4 -- PDF](/files/2sem2021/slides/topico_4.pdf)

### Tópico 5

* [Slides do tópico 5 -- HTML](/files/2sem2021/slides/topico_5.html)
* [Slides do tópico 5 -- PDF](/files/2sem2021/slides/topico_5.pdf)

### Tópico 6

* [Slides do tópico 6 -- HTML](/files/2sem2021/slides/topico_6.html)
* [Slides do tópico 6 -- PDF](/files/2sem2021/slides/topico_6.pdf)

### Tópico 7

* [Slides do tópico 7 -- HTML](/files/2sem2021/slides/topico_7.html)
* [Slides do tópico 7 -- PDF](/files/2sem2021/slides/topico_7.pdf)

### Tópico 8

* [Slides do tópico 8 -- HTML](/files/2sem2021/slides/topico_8.html)
* [Slides do tópico 8 -- PDF](/files/2sem2021/slides/topico_8.pdf)


## Conjunto de dados

* [empresa.xlsx](/files/2sem2021/datasets/empresa.xlsx)
* [iris.xlsx](/files/2sem2021/datasets/iris.xlsx)
* [mtcars.csv](/files/2sem2021/datasets/mtcars.csv)
* [peixe_descoberto.xlsx](/files/2sem2021/datasets/peixe_descoberto.xlsx)
* [religiao_renda.csv](/files/2sem2021/datasets/religiao_renda.csv)
* [religiao_renda.xlsx](/files/2sem2021/datasets/religiao_renda.xlsx)
* [ToothGrowth.ods](/files/2sem2021/datasets/ToothGrowth.ods)
* [cancer_pulmao.csv](/files/2sem2021/datasets/cancer_pulmao.csv)
* [cancer_pulmao.xlsx](/files/2sem2021/datasets/cancer_pulmao.xlsx)


## Listas de exercícios

* [Lista 5](/files/2sem2021/lista/matd46/lista_5.pdf)
* [Lista 6](/files/2sem2021/lista/matd46/lista_6.pdf)
* [Lista 7](/files/2sem2021/lista/matd46/lista_7.pdf)
* [Lista 8](/files/2sem2021/lista/matd46/lista_8.pdf)

### Conjunto de dados das listas de exercícios

* [carros.xlsx](/files/2sem2021/lista/matd46/datasets/carros.xlsx)
* [empresa.xlsx](/files/2sem2021/lista/matd46/datasets/empresa.xlsx)
* [equipe_sample__Palmas.xlsx](/files/2sem2021/lista/matd46/datasets/equipe_sample__Palmas.xlsx)
* [iris.ods](/files/2sem2021/lista/matd46/datasets/iris.ods)
* [marketing.csv](/files/2sem2021/lista/matd46/datasets/marketing.csv)
* [mtcars.csv](/files/2sem2021/lista/matd46/datasets/mtcars.csv)


## Cronograma

* [Cronograma](/files/2sem2021/cronograma/estatistica-computacional-cronograma.xlsx)

------

# Métodos Estatísticos (Código MAT236)

Material didático de apoido e de suporte a disciplina **MAT236** com título **Métodos Estatísticos**.

## Primeiro dia

* [Primeiro dia](/files/2sem2021/slides/mat236/primeiroDia.html)
* [Primeiro dia](/files/2sem2021/slides/mat236/primeiroDia.pdf)

## Unidades 1, 2 e 3

* [Slides -- Unidades 1, 2 e 3 -- gráficos](/files/2sem2021/slides/mat236/graficos.pdf)
* [Slides -- Unidades 1, 2 e 3 -- medidas de resumo](/files/2sem2021/slides/mat236/medidasResumo.pdf)

## Unidade 4

* [Slides -- Unidade 4 -- introduação à probabilidade](/files/2sem2021/slides/mat236/introProb.pdf)
* [Slides -- Unidade 4 -- variável contínua](/files/2sem2021/slides/mat236/varContinua.pdf)
* [Slides -- Unidade 4 -- variável discreta](/files/2sem2021/slides/mat236/varDiscreta.pdf)

## Unidade 5

* [Slides -- Unidade 5 -- intervalo de confiança](/files/2sem2021/slides/mat236/intervaloConfianca.pdf)
* [slides -- Unidade 5 -- teste de hipóteses para uma população](/files/2sem2021/slides/mat236/testeHipoteses.pdf)

## Conjuntos de dados

* [companhia_MB.xlsx](/files/2sem2021/datasets/companhia_MB.xlsx)

## Listas de exercícios

* [Primeira lista de exercício](/files/2sem2021/lista/mat236/lista_est_descr.pdf)
* [Segunda lista de exercícios](/files/2sem2021/lista/mat236/lista_prob.pdf)
* [Terceira lista de exercícios](/files/2sem2021/lista/mat236/lista-ex-ic.pdf)
* [Quarta lista de exercícios](/files/2sem2021/lista/mat236/lista-test-1-pop.pdf)
* [Quinta lista de exercícios](/files/2sem2021/lista/mat236/lista-test-1-pop-part2.pdf)

## Livros 

* [Estatística básica -- Bussab e Morettin](/files/2sem2021/livros/estatistica-basica-bussab-morettin.pdf)
* [Estatística Aplicada às Ciências Sociais -- Barbetta](/files/2sem2021/livros/estatistica-aplicada-barbetta.pdf)
* [Applied Statistics and Probability for Engineers](/files/2sem2021/livros/applied-statistics-montgomery.pdf)
* [Computation Statistics Handbook with MATLAB](/files/2sem2021/livros/computational-statistics-matlab.pdf)

## Notas de aulas


### dia 17/08/2021

* [Notas de aulas (.xlsx)](/files/2sem2021/notas/mat236/mes-08-dia-17.xlsx)
* [Notas de aulas (.pdf)](/files/2sem2021/notas/mat236/mes-08-dia-17.pdf)

### dia 24/08/2021

* [Notas de aulas (.xlsx)](/files/2sem2021/notas/mat236/mes-08-dia-24.xlsx)
* [Notas de aulas (.pdf)](/files/2sem2021/notas/mat236/mes-08-dia-24.pdf)

### dia 31/08/2021

* [Notas de aulas (.xlsx)](/files/2sem2021/notas/mat236/mes-08-dia-31.xlsx)
* [Notas de aulas (.pdf)](/files/2sem2021/notas/mat236/mes-08-dia-31.pdf)

### dia 14/09/2021

* [Notas de aula (.pdf)](/files/2sem2021/notas/mat236/mes-09-dia-14.pdf)

### dia 21/09/2021

* [Notas de aulas (.xlsx)](/files/2sem2021/notas/mat236/mes-09-dia-21.xlsx)
* [Notas de aulas (.pdf)](/files/2sem2021/notas/mat236/mes-09-dia-21.pdf)

### dia 28/09/2021

[Notas de aulas (.pdf)](/files/2sem2021/notas/mat236/mes-09-dia-28.pdf)

### Dia 05/10/2021

[Notas de aula (.pdf)](/files/2sem2021/notas/mat236/mes-10-dia-05.pdf)

### Dia 21/10/2021

[Notas de aula (.pdf)](/files/2sem2021/notas/mat236/mes-10-dia-21.pdf)

### Dia 26/10/2021

[Notas de aula (.pdf)](/files/2sem2021/notas/mat236/mes-10-dia-26.pdf)

## Notas do Fórum 

* [Notas do fórum (.xlsx) -- 15/08/2021 à 21/08/2021](/files/2sem2021/log-forum/semana_15_08_2021_a_21_08_2021_ava.xlsx)
* [Notas do fórum (.xlsx) -- 22/08/2021 a 28/08/2021](/files/2sem2021/log-forum/semana_22_08_2021_a_28_08_2021_ava.xlsx)
* [Notas do fórum (.xlsx) -- 29/08/2021 a 04/09/2021](/files/2sem2021/log-forum/semana_29_08_2021_a_04_09_2021_ava.xlsx)
* [Notas do fórum (.xlsx) -- 05/09/2021 a 11/09/2021](/files/2sem2021/log-forum/semana_05_09_2021_a_11_09_2021_ava.xlsx)
* [Notas do fórum (.xlsx) -- 12/09/2021 a 18/09/2021](/files/2sem2021/log-forum/semana_12_09_2021_a_18_09_2021_ava.xlsx)
* [Notas do fórum (.xlsx) -- 19/09/2021 a 25/09/2021](/files/2sem2021/log-forum/semana_19_09_2021_a_25_09_2021_ava.xlsx)
* [Notas do fórum (.xlsx) -- 26/09/2021 a 02/10/2021](/files/2sem2021/log-forum/semana_26_09_2021_a_02_10_2021_ava.xlsx)
* [Notas do fórum (.xlsx) -- 03/10/2021 a 09/10/2021](/files/2sem2021/log-forum/semana_03_10_2021_a_09_10_2021_ava.xlsx)
* [Notas do fórum (.xlsx) -- 17/10/2021 a 23/10/2021](/files/2sem2021/log-forum/semana_17_10_2021_a_23_10_2021_ava.xlsx)
* [Notas do fórum (.xlsx) -- 24/10/2021 a 30/10/2021](/files/2sem2021/log-forum/semana_24_10_2021_a_30_10_2021_ava.xlsx)
* [Notas do fórum (.xlsx) -- 31/10/2021 a 06/11/2021](/files/2sem2021/log-forum/semana_31_10_2021_a_06_11_2021_ava.xlsx)
* [Notas do fórum (.xlsx) -- 07/11/2021 a 13/11/2021](/files/2sem2021/log-forum/semana_07_11_2021_a_13_11_2021_ava.xlsx)
* [Notas do fórum (.xlsx) -- 14/11/2021 a 20/11/2021](/files/2sem2021/log-forum/semana_14_11_2021_a_20_11_2021_ava.xlsx)
* [Notas do fórum (.xlsx) -- 21/11/2021 a 27/11/2021](/files/2sem2021/log-forum/semana_21_11_2021_a_27_11_2021_ava.xlsx)



## Tabelas

* [Tabela z](/files/2sem2021/slides/mat236/tabela-z.pdf)
* [Tabela t](/files/2sem2021/slides/mat236/tabela-t-student.pdf)
* [Tabela qui-quadrado](/files/2sem2021/slides/mat236/tabela-chi-square.pdf)

## Gabaritos das provas

* [Gabarito da primeira prova](/files/2sem2021/gabaritos/mat236/gabarito-p1.pdf)

--------

# Dados ENEM 2019

### Dicionário de Variáveis

[Dicionário de Variáveis -- INEP -- 2019](/files/2sem2021/datasets/dicionario_variaveis_inep.xlsx).

### .csv

* [Rio Branco](/files/2sem2021/trabalho-final/equipe_sample__Rio_Branco.csv)
* [Maceió](/files/2sem2021/trabalho-final/equipe_sample__Maceio.csv)
* [Macapá](/files/2sem2021/trabalho-final/equipe_sample__Macapa.csv)
* [Manaus](/files/2sem2021/trabalho-final/equipe_sample__Manaus.csv)
* [Salvador](/files/2sem2021/trabalho-final/equipe_sample__Salvador.csv)
* [Fortaleza](/files/2sem2021/trabalho-final/equipe_sample__Fortaleza.csv)
* [Brasília](/files/2sem2021/trabalho-final/equipe_sample__Brasilia.csv)
* [Vitória](/files/2sem2021/trabalho-final/equipe_sample__Vitoria.csv)
* [Goiânia](/files/2sem2021/trabalho-final/equipe_sample__Goiania.csv)
* [São Luís](/files/2sem2021/trabalho-final/equipe_sample__Sao_Luis.csv)
* [Cuiabá](/files/2sem2021/trabalho-final/equipe_sample__Cuiaba.csv)
* [Campo Grande](/files/2sem2021/trabalho-final/equipe_sample__Campo_Grande.csv)
* [Belo Horizonte](/files/2sem2021/trabalho-final/equipe_sample__Belo_Horizonte.csv)
* [Belém](/files/2sem2021/trabalho-final/equipe_sample__Belem.csv)
* [João Pessoa](/files/2sem2021/trabalho-final/equipe_sample__Joao_Pessoa.csv)
* [Curitiba](/files/2sem2021/trabalho-final/equipe_sample__Curitiba.csv)
* [Recife](/files/2sem2021/trabalho-final/equipe_sample__Recife.csv)
* [Teresina](/files/2sem2021/trabalho-final/equipe_sample__Teresina.csv)
* [Rio de Janeiro](/files/2sem2021/trabalho-final/equipe_sample__Rio_de_Janeiro.csv)
* [Natal](/files/2sem2021/trabalho-final/equipe_sample__Natal.csv)
* [Porto Alegre](/files/2sem2021/trabalho-final/equipe_sample__Porto_Alegre.csv)
* [Porto Velho](/files/2sem2021/trabalho-final/equipe_sample__Porto_Velho.csv)
* [Boa Vista](/files/2sem2021/trabalho-final/equipe_sample__Boa_Vista.csv)
* [Florianópolis](/files/2sem2021/trabalho-final/equipe_sample__Florianopolis.csv)
* [São Paulo](/files/2sem2021/trabalho-final/equipe_sample__Sao_Paulo.csv)
* [Aracaju](/files/2sem2021/trabalho-final/equipe_sample__Aracaju.csv)
* [Palmas](/files/2sem2021/trabalho-final/equipe_sample__Palmas.csv)

### .xlsx

* [Rio Branco](/files/2sem2021/trabalho-final/equipe_sample__Rio_Branco.xlsx)
* [Maceió](/files/2sem2021/trabalho-final/equipe_sample__Maceio.xlsx)
* [Macapá](/files/2sem2021/trabalho-final/equipe_sample__Macapa.xlsx)
* [Manaus](/files/2sem2021/trabalho-final/equipe_sample__Manaus.xlsx)
* [Salvador](/files/2sem2021/trabalho-final/equipe_sample__Salvador.xlsx)
* [Fortaleza](/files/2sem2021/trabalho-final/equipe_sample__Fortaleza.xlsx)
* [Brasília](/files/2sem2021/trabalho-final/equipe_sample__Brasilia.xlsx)
* [Vitória](/files/2sem2021/trabalho-final/equipe_sample__Vitoria.xlsx)
* [Goiânia](/files/2sem2021/trabalho-final/equipe_sample__Goiania.xlsx)
* [São Luís](/files/2sem2021/trabalho-final/equipe_sample__Sao_Luis.xlsx)
* [Cuiabá](/files/2sem2021/trabalho-final/equipe_sample__Cuiaba.xlsx)
* [Campo Grande](/files/2sem2021/trabalho-final/equipe_sample__Campo_Grande.xlsx)
* [Belo Horizonte](/files/2sem2021/trabalho-final/equipe_sample__Belo_Horizonte.xlsx)
* [Belém](/files/2sem2021/trabalho-final/equipe_sample__Belem.xlsx)
* [João Pessoa](/files/2sem2021/trabalho-final/equipe_sample__Joao_Pessoa.xlsx)
* [Curitiba](/files/2sem2021/trabalho-final/equipe_sample__Curitiba.xlsx)
* [Recife](/files/2sem2021/trabalho-final/equipe_sample__Recife.xlsx)
* [Teresina](/files/2sem2021/trabalho-final/equipe_sample__Teresina.xlsx)
* [Rio de Janeiro](/files/2sem2021/trabalho-final/equipe_sample__Rio_de_Janeiro.xlsx)
* [Natal](/files/2sem2021/trabalho-final/equipe_sample__Natal.xlsx)
* [Porto Alegre](/files/2sem2021/trabalho-final/equipe_sample__Porto_Alegre.xlsx)
* [Porto Velho](/files/2sem2021/trabalho-final/equipe_sample__Porto_Velho.xlsx)
* [Boa Vista](/files/2sem2021/trabalho-final/equipe_sample__Boa_Vista.xlsx)
* [Florianópolis](/files/2sem2021/trabalho-final/equipe_sample__Florianopolis.xlsx)
* [São Paulo](/files/2sem2021/trabalho-final/equipe_sample__Sao_Paulo.xlsx)
* [Aracaju](/files/2sem2021/trabalho-final/equipe_sample__Aracaju.xlsx)
* [Palmas](/files/2sem2021/trabalho-final/equipe_sample__Palmas.xlsx)

### .RData

* [Rio Branco](/files/2sem2021/trabalho-final/equipe_sample__Rio_Branco.RData)
* [Maceió](/files/2sem2021/trabalho-final/equipe_sample__Maceio.RData)
* [Macapá](/files/2sem2021/trabalho-final/equipe_sample__Macapa.RData)
* [Manaus](/files/2sem2021/trabalho-final/equipe_sample__Manaus.RData)
* [Salvador](/files/2sem2021/trabalho-final/equipe_sample__Salvador.RData)
* [Fortaleza](/files/2sem2021/trabalho-final/equipe_sample__Fortaleza.RData)
* [Brasília](/files/2sem2021/trabalho-final/equipe_sample__Brasilia.RData)
* [Vitória](/files/2sem2021/trabalho-final/equipe_sample__Vitoria.RData)
* [Goiânia](/files/2sem2021/trabalho-final/equipe_sample__Goiania.RData)
* [São Luís](/files/2sem2021/trabalho-final/equipe_sample__Sao_Luis.RData)
* [Cuiabá](/files/2sem2021/trabalho-final/equipe_sample__Cuiaba.RData)
* [Campo Grande](/files/2sem2021/trabalho-final/equipe_sample__Campo_Grande.RData)
* [Belo Horizonte](/files/2sem2021/trabalho-final/equipe_sample__Belo_Horizonte.RData)
* [Belém](/files/2sem2021/trabalho-final/equipe_sample__Belem.RData)
* [João Pessoa](/files/2sem2021/trabalho-final/equipe_sample__Joao_Pessoa.RData)
* [Curitiba](/files/2sem2021/trabalho-final/equipe_sample__Curitiba.RData)
* [Recife](/files/2sem2021/trabalho-final/equipe_sample__Recife.RData)
* [Teresina](/files/2sem2021/trabalho-final/equipe_sample__Teresina.RData)
* [Rio de Janeiro](/files/2sem2021/trabalho-final/equipe_sample__Rio_de_Janeiro.RData)
* [Natal](/files/2sem2021/trabalho-final/equipe_sample__Natal.RData)
* [Porto Alegre](/files/2sem2021/trabalho-final/equipe_sample__Porto_Alegre.RData)
* [Porto Velho](/files/2sem2021/trabalho-final/equipe_sample__Porto_Velho.RData)
* [Boa Vista](/files/2sem2021/trabalho-final/equipe_sample__Boa_Vista.RData)
* [Florianópolis](/files/2sem2021/trabalho-final/equipe_sample__Florianopolis.RData)
* [São Paulo](/files/2sem2021/trabalho-final/equipe_sample__Sao_Paulo.RData)
* [Aracaju](/files/2sem2021/trabalho-final/equipe_sample__Aracaju.RData)
* [Palmas](/files/2sem2021/trabalho-final/equipe_sample__Palmas.RData)