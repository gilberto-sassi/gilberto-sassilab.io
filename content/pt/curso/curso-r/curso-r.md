---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Introdução à Estatística usando o R: Seja bem-vind@ ao tidyverse"
subtitle: ""
summary: ""
authors: []
tags: []
categories: []
date: 2020-08-01T13:24:11-03:00
lastmod: 2020-08-01T13:24:11-03:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

O objetivo desse curso é usar o `R` para apresentar conceitos básicos de Estatística. O R é um software livre, gratuito e fácil de aprender e usar. O `R` é um linguagem derivada da linguagem `S` que foi proposta John Chamber em 1976 pelo *Bell Labs*. A ideia da linguagem S era permitir que profissionais analisassem conjuntos de dados mesmo com pouco conhecimento de programação. O `R` foi lançamento em 1991 por Ross Ihaka e Robert Gentleman na Nova Zelândia e a adesão ao `R` tem aumentado a cada dia, principalmente por causa da constante atualização do *software*, da portabilidade e da grande comunidade de usuários e desenvolvedores que adicionam novas funcionalidades ao `R` através de pacotes. Nesse curso, vamos introduzir o `R` e os conceitos básicos de Estatística usando conjuntos de dados reais. Além disso, vamos usar os pacotes incluídos no `tidyverse`, principalmente os pacotes `dplyr` e `ggplot2`. As aulas terão duração de quatro horas e acontecerão em seis sábados no laboratório 143 do IME-UFBA.

## Slides

[Aula 1](/files/curso-r-tidyverse/aula_1.pdf) <br/>
[Aula 2](/files/curso-r-tidyverse/aula_2.pdf) <br/>
[Aula 3](/files/curso-r-tidyverse/aula_3.pdf) <br/>
[Aula 4](/files/curso-r-tidyverse/aula_4.pdf) <br/>
[Aula 5](/files/curso-r-tidyverse/aula_5.pdf)

## Conjunto de dados

[dados.xlsx](/files/curso-r-tidyverse/dados.xlsx) <br/>
[iris.csv](/files/curso-r-tidyverse/iris.csv) <br/>
[iris.txt](/files/curso-r-tidyverse/iris.txt) 
